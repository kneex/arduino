void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(2, INPUT);
  pinMode(3, INPUT);
  pinMode(4, INPUT);
  pinMode(8, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
    //tone(8, 1000);
    int A = digitalRead(2);
    int B = digitalRead(3);
    int C = digitalRead(4);
    if (A==1 && B == 0 && C==0) {
      tone(8,262);
    }
    else if (A == 0 && B == 0 && C== 0) {
      noTone(8);
    }

    if (A==0 && B == 1 && C==0) {
      tone(8,294);
    }
    else if (A == 0 && B == 0 && C== 0) {
      noTone(8);
    }
    
    if (A==0 && B == 0 && C==1) {
      tone(8,330);
    }
    else if (A == 0 && B == 0 && C== 0) {
      noTone(8);
    }
        
    Serial.print(A);
    Serial.print(B);
    Serial.println(C);
    delay(100);
}
