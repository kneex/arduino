#include <Servo.h>

Servo engine;
int const potarPin = A5;
int potarVal;
int angle;

void setup() {
  // put your setup code here, to run once:
  engine.attach(9);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  potarVal = analogRead(potarPin);
  Serial.print("valeur du potard = ");
  Serial.print(potarVal);
  angle = map(potarVal, 0, 1023, 0, 179);
  Serial.print("angle = ");
  Serial.println(angle);
  engine.write(angle);
  delay(15);
}
