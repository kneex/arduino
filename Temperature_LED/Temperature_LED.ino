void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  for (int i=2; i<5; i++){
    pinMode(i, OUTPUT);
    digitalWrite(i, LOW);
  }
}
const int sensorPin = A1;
const float T_ref = 18.0;

void loop() {
  // put your main code here, to run repeatedly:
  int sensorVal = analogRead(sensorPin);
  Serial.println(sensorVal);
  float voltage = (sensorVal/1024.0)*5;
  Serial.print("La tension est de ");
  Serial.println(voltage);
  float T = (voltage-0.5)*100;
  Serial.print("La temperature est de ");
  Serial.println(T);
  if (T<=T_ref+2){
    digitalWrite(4, HIGH);
    digitalWrite(3, LOW);
  }
  else if (T>T_ref+2 && T<T_ref+4){
    digitalWrite(3, HIGH);
    digitalWrite(4,LOW);
  }
  else if (T>=T_ref+4){
    digitalWrite(2, HIGH);
    digitalWrite(3,LOW);
  }
  delay(100);
}
